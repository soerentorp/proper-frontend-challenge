import PropTypes from 'prop-types'
import {Text} from 'components'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'

const Avatar = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="avatar">
      <Text text={props.initials} />
      <style jsx>{`
        .avatar {
          width: 32px;
          height: 32px;
          border-radius: 9999px;
          border: solid 2px black;
          display: flex;
          align-items: center;
          justify-content: center;
          font-size: 16px;
          font-weight: bold;
          background-color: black;
          color: #eee;
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

Text.propTypes = {
  initials: PropTypes.string,
  ...cssFromPropsTypes,
}

export default Avatar
