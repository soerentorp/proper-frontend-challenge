import PropTypes from 'prop-types'
import {Text} from 'components'
import THEME from 'theme'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'

const Button = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <button className={`${props.type} ${props.selected ? 'selected' : ''}`} onClick={props.onClick}>
      <Text text={props.text} size="large" weight="bold" />
      <style jsx>{`
        button {
          border: none;
          padding-left: 24px;
          padding-right: 24px;
          height: 48px;
          display: block;
          border-radius: 8px;
          transition: all 0.25s ease-in-out;
          ${cssFromProps}
        }
        .primary {
          color: white;
          background-color: ${THEME.black};
        }
        .primary:hover {
          background-color: ${THEME.red};
        }
        .primary:active {
          background-color: ${THEME.black};
        }
        .secondary {
          color: black;
          background-color: white;
          border: solid 1px black;
        }
        .secondary:hover {
          border-color: ${THEME.red};
          color: ${THEME.red};
        }
        .secondary:active {
          border-color: ${THEME.black};
          color: ${THEME.black};
        }
        .secondary.selected {
          color: white;
          background-color: black;
          border: solid 1px black;
        }
      `}</style>
    </button>
  )
}

Button.defaultProps = {
  type: 'secondary',
}

Button.propTypes = {
  type: PropTypes.oneOf(['primary', 'secondary']).isRequired,
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  ...cssFromPropsTypes,
}

export default Button
