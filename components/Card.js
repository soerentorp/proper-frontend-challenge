import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import THEME from 'theme'

const Card = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="card">
      {props.children}
      <style jsx>{`
        .card {
          border: solid 1px ${THEME.borderColor};
          border-radius: 8px;
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

Card.propTypes = {
  ...cssFromPropsTypes,
}

export default Card
