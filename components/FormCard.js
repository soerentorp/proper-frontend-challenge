import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import THEME from 'theme'

const FormCard = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="form-card">
      {props.children}
      <style jsx>{`
        .form-card {
          margin-top: 36px;
          border: solid 1px ${THEME.borderColor};
          padding: 48px;
          border-radius: 8px;
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

FormCard.propTypes = {
  ...cssFromPropsTypes,
}

export default FormCard
