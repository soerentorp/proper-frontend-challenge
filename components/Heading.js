import PropTypes from 'prop-types'
import {Text} from 'components'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'

const Heading = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <h1 className="heading">
      <Text {...props} size="hugest" weight="fat" />
      <style jsx>{`
        .heading {
          ${cssFromProps}
        }
      `}</style>
    </h1>
  )
}

Heading.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  ...cssFromPropsTypes,
}

export default Heading
