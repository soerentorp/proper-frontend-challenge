import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import {Text} from 'components'

const Label = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="label">
      <Text text={props.text} weight="bold" />
      <style jsx>{`
        .label {
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

Label.propTypes = {
  ...cssFromPropsTypes,
}

export default Label
