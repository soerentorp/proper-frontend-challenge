import PropTypes from 'prop-types'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import {Button} from 'components'
import Link from 'next/link'

const LinkButton = (props) => {
  const {href, locale, ...tail} = props
  return (
    <Link href={href} locale={locale}>
      <div className="button">
        <Button {...tail} />
        <style jsx>
          {`
            .button {
              display: inline-block;
            }
          `}
        </style>
      </div>
    </Link>
  )
}

LinkButton.propTypes = {
  href: PropTypes.string.isRequired,
  locale: PropTypes.oneOf(['da', 'en']),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  ...cssFromPropsTypes,
}

export default LinkButton
