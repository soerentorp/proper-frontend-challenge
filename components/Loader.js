import PuffLoader from 'react-spinners/PuffLoader'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'

const Loader = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="loader">
      <PuffLoader size={48} color={'#000'} loading />
      <style jsx>{`
        .loader {
          display: flex;
          justify-content: center;
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

Loader.propTypes = {
  ...cssFromPropsTypes,
}

export default Loader
