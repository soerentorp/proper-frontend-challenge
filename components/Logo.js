import Link from 'next/link'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'

const Logo = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="logo">
      <Link href="/">PFC</Link>
      <style jsx>
        {`
          .logo {
            font-weight: 900;
            font-size: 21px;
            ${cssFromProps}
          }
        `}
      </style>
    </div>
  )
}

Logo.propTypes = {
  ...cssFromPropsTypes,
}

export default Logo
