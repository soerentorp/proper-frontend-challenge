import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import THEME from 'theme'
import {Card, Text} from 'components'

const NotFound = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <div className="not-found">
      <Card padding={40}>
        <Text text={{id: '@t.not_found@@'}} size="larger" />
      </Card>
      <style jsx>{`
        .not-found {
          text-align: center;
          ${cssFromProps}
        }
      `}</style>
    </div>
  )
}

NotFound.propTypes = {
  ...cssFromPropsTypes,
}

export default NotFound
