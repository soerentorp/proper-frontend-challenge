import {Text, Logo, TextField, Avatar} from 'components'
import Link from 'next/link'
import {useRouter} from 'next/router'
import THEME from 'theme'

const Page = (props) => {
  const router = useRouter()
  return (
    <div>
      <div className="nav">
        <Logo />
        <div className="menu">
          <Link href="/">
            <a className="nav-item">
              <Text text={{id: '@t.nav.dashboard@@'}} weight={router.pathname == '/' ? 'fat' : ''} size="large" />
            </a>
          </Link>
          <Link href="/units">
            <a className="nav-item">
              <Text
                text={{id: '@t.nav.units@@'}}
                weight={router.pathname.includes('units') ? 'fat' : ''}
                size="large"
              />
            </a>
          </Link>
        </div>
        <Link href="/user">
          <a>
            <Avatar initials="JF" />
          </a>
        </Link>
      </div>
      <div className="main-wrapper">
        <main>{props.children}</main>
      </div>
      <style jsx>{`
        .nav {
          height: 64px;
          padding-left: 24px;
          padding-right: 24px;
          border-bottom: solid 1px ${THEME.borderColor};
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
        .menu {
          display: flex;
          align-items: center;
        }
        .nav-item {
          padding-left: 32px;
          padding-right: 32px;
          height: 64px;
          display: flex;
          align-items: center;
          transition: all 0.25s ease-in-out;
        }
        .nav-item:hover {
          background-color: #efefef;
        }
        .nav-item:active {
          background-color: #ccc;
        }
        .main-wrapper {
          display: flex;
          justify-content: center;
        }
        main {
          padding: 48px;
          max-width: 960px;
          flex: 1;
        }
      `}</style>
    </div>
  )
}

export default Page
