import PropTypes from 'prop-types'
import {useText} from 'hooks'
import {useCssFromProps} from 'hooks'
import THEME from 'theme'

const Text = ({text, children, ...props}) => {
  const cssFromProps = useCssFromProps(props)
  const content = useText(text) || children

  if (!content) return null

  return (
    <div className={`text size-${props.size} weight-${props.weight}`}>
      {content}
      <style jsx>{`
        .text {
          ${cssFromProps}
        }
        .text.size-small {
          font-size: ${THEME.fontSize.small}px;
        }
        .text.size-regular {
          font-size: ${THEME.fontSize.regular}px;
        }
        .text.size-large {
          font-size: ${THEME.fontSize.large}px;
        }
        .text.size-larger {
          font-size: ${THEME.fontSize.larger}px;
        }
        .text.size-largest {
          font-size: ${THEME.fontSize.largest}px;
        }
        .text.size-huge {
          font-size: ${THEME.fontSize.huge}px;
        }
        .text.size-huger {
          font-size: ${THEME.fontSize.huger}px;
        }
        .text.size-hugest {
          font-size: ${THEME.fontSize.hugest}px;
        }
        .text.weight-regular {
          font-weight: ${THEME.fontWeight.regular};
        }
        .text.weight-bold {
          font-weight: ${THEME.fontWeight.bold};
        }
        .text.weight-fat {
          font-weight: ${THEME.fontWeight.fat};
        }
      `}</style>
    </div>
  )
}

Text.defaultProps = {
  size: 'regular',
  weight: 'regular',
}

Text.propTypes = {
  size: PropTypes.oneOf(['regular', 'large', 'larger', 'largest', 'huge', 'huger', 'hugest', 'light']).isRequired,
  weight: PropTypes.oneOf(['regular', 'bold', 'fat']).isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
}

export default Text
