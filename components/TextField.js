import PropTypes from 'prop-types'
import {useCssFromProps} from 'hooks'
import cssFromPropsTypes from 'lib/cssFromPropsTypes'
import THEME from 'theme'

const TextField = (props) => {
  const cssFromProps = useCssFromProps(props)

  return (
    <>
      <input
        className="text-field"
        onChange={props.onChange}
        value={props.value}
        placeholder={props.placeholder}
        type="text"
      />
      <style jsx>{`
        .text-field {
          font-size: ${THEME.fontSize.large}px;
          font-family: ${THEME.fontFamily};
          font-weight: ${THEME.fontWeight.regular};
          border: solid 1px ${THEME.borderColor};
          border-radius: 4px;
          -webkit-appearance: none;
          height: 48px;
          padding-right: 12px;
          padding-left: 12px;
          outline: none;
          ${cssFromProps}
        }
      `}</style>
    </>
  )
}

TextField.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  ...cssFromPropsTypes,
}

export default TextField
