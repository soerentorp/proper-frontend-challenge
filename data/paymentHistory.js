export default [
  {
    month: '2020-01-01',
    due: 0,
    paid: 36451,
  },
  {
    month: '2020-02-01',
    due: 0,
    paid: 37012,
  },
  {
    month: '2020-03-01',
    due: 0,
    paid: 37521,
  },
  {
    month: '2020-04-01',
    due: 0,
    paid: 28948,
  },
  {
    month: '2020-05-01',
    due: 0,
    paid: 39184,
  },
  {
    month: '2020-06-01',
    due: 0,
    paid: 39484,
  },
  {
    month: '2020-07-01',
    due: 0,
    paid: 39984,
  },
  {
    month: '2020-08-01',
    due: 0,
    paid: 27391,
  },
  {
    month: '2020-09-01',
    due: 0,
    paid: 42391,
  },
  {
    month: '2020-10-01',
    due: 0,
    paid: 42391,
  },
  {
    month: '2020-11-01',
    due: 8178,
    paid: 34641,
  },
  {
    month: '2020-12-01',
    due: 44923,
    paid: 0,
  },
]
