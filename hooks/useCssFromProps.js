import React from 'react'

const useCssFromProps = (props) => {
  const style = React.useMemo(() => {
    let out = ''
    if (props.margin) out += `margin: ${props.margin}px;`
    if (props.marginTop) out += `margin-top: ${props.marginTop}px;`
    if (props.marginRight) out += `margin-right: ${props.marginRight}px;`
    if (props.marginBottom) out += `margin-bottom: ${props.marginBottom}px;`
    if (props.marginLeft) out += `margin-left: ${props.marginLeft}px;`

    if (props.padding) out += `padding: ${props.padding}px;`
    if (props.paddingTop) out += `padding-top: ${props.paddingTop}px;`
    if (props.paddingRight) out += `padding-right: ${props.paddingRight}px;`
    if (props.paddingBotom) out += `padding-bottom: ${props.paddingBottom}px;`
    if (props.paddingLeft) out += `padding-left: ${props.paddingLeft}px;`
    return out
  }, [
    props.margin,
    props.marginTop,
    props.marginRight,
    props.marginBottom,
    props.marginLeft,

    props.padding,
    props.paddingTop,
    props.paddingRight,
    props.paddingBottom,
    props.paddingLeft,
  ])
  return style
}

export default useCssFromProps
