import {useIntl} from 'react-intl'

const useText = (text) => {
  const intl = useIntl()
  if (!text) return null
  return typeof text === 'object' ? intl.formatMessage({id: text.id}, text.values) : text
}

export default useText
