import {ApolloClient, HttpLink, InMemoryCache} from '@apollo/client'

let apolloClient

const createApolloClient = () => {
  return new ApolloClient({
    uri: process.env.NEXT_PUBLIC_GRAPHQL_ENDPOINT,
    cache: new InMemoryCache(),
  })
}

export const useApollo = () => {
  if (apolloClient) return apolloClient
  apolloClient = createApolloClient()
  return apolloClient
}
