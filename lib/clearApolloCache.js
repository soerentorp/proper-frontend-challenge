const clearApolloCache = (cache) => {
  cache.evict({
    id: 'ROOT_QUERY',
    fieldName: 'units',
    broadcast: true,
  })
  cache.evict({
    id: 'ROOT_QUERY',
    fieldName: 'dashboard',
    broadcast: true,
  })
}

export default clearApolloCache
