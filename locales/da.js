export default {
  '@t.shared.save@@': 'Gem',
  '@t.shared.edit@@': 'Ret',
  '@t.shared.create@@': 'Opret',
  '@t.not_found@@': 'Beklager, kunne ikke finde indholdet.',

  '@t.nav.dashboard@@': 'Dashboard',
  '@t.nav.units@@': 'Lejemål',

  '@t.dashboard.title@@': 'Dashboard',
  '@t.dashboard.rent_due.caption@@': 'Leje tilgode denne måned',
  '@t.dashboard.rent_due.billed_caption@@': 'Opkrævet',
  '@t.dashboard.rent_due.paid_caption@@': 'Betalt',
  '@t.dashboard.occupancy.caption@@': 'Procent udlejet',
  '@t.dashboard.occupancy.occupied_caption@@': 'Udlejet',
  '@t.dashboard.occupancy.vacant_caption@@': 'Tomgang',
  '@t.dashboard.occupancy.total_caption@@': 'Total',
  '@t.dashboard.payments.caption@@': 'Betalinger',
  '@t.dashboard.chart.legend@@': `{key, select,
    due {Tilgode}
    paid {Betalt}
  }`,

  '@t.units.title@@': 'Lejemål',
  '@t.units.add_button@@': 'Tilføj nyt lejemål',
  '@t.units.blank_slate@@': 'Ingen lejemål oprettet.',
  '@t.units.status@@': `{status, select, 
    occupied {Udlejet}
    vacant {Tomgang}
  }`,

  '@t.unit.title@@': 'Lejemål',
  '@t.unit.back_to_units@@': 'Tilbage til lejemål',
  '@t.unit.tenant_caption@@': 'Lejer',
  '@t.unit.vacant_caption@@': 'Lejemålet er i øjeblikket i tomgang',
  '@t.unit.vacant_info@@': 'Add a tenant to this unit to start a new tenancy.',
  '@t.unit.vacant_info@@': 'Tilføj en lejer til lejemålet for at starte en ny udlejning.',
  '@t.unit.add_tenant@@': 'Tilføj lejer',
  '@t.unit.lease_caption@@': 'Leje',
  '@t.unit.unit_caption@@': 'Lejemål',
  '@t.unit.delete_unit@@': 'Slet lejemål',
  '@t.unit.end_tenancy@@': 'Ophør udlejning',
  '@t.unit.lease_title@@': 'Leje',
  '@t.unit.back_to_unit@@': 'Tilbage til lejemål',
  '@t.unit.lease_label@@': 'Årlig leje',
  '@t.unit.tenant_title@@': 'Ljer',
  '@t.unit.tenant.name_label@@': 'Navn',
  '@t.unit.tenant.email_label@@': 'E-mail',
  '@t.unit.tenant.phone_label@@': 'Telefon',
  '@t.unit.tenant.validation_message@@': 'Udfyld venligst navn, e-mail og telefon.',
  '@t.unit.unit_title@@': 'Lejemål',
  '@t.unit.unit.size_label@@': 'Areal',
  '@t.unit.unit.rooms_label@@': 'Antal værelser',
  '@t.unit.unit.utilities_label@@': 'Faciliteter',

  '@t.new_unit.title@@': 'Nyt lejemål',
  '@t.new_unit.address_label@@': 'Adresse',
  '@t.new_unit.search_address@@': 'Søg adresse',
  '@t.new_unit.validation_message@@': 'Vælg venligst en adresse fra listen.',

  '@t.user.caption@@': 'Vælg dit foretrukne sprog',
}
