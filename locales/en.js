export default {
  '@t.shared.save@@': 'Save',
  '@t.shared.edit@@': 'Edit',
  '@t.shared.create@@': 'Create',
  '@t.not_found@@': 'Sorry, could not find content.',

  '@t.nav.dashboard@@': 'Dashboard',
  '@t.nav.units@@': 'Units',

  '@t.dashboard.title@@': 'Dashboard',
  '@t.dashboard.rent_due.caption@@': 'Rent due this month',
  '@t.dashboard.rent_due.billed_caption@@': 'Billed',
  '@t.dashboard.rent_due.paid_caption@@': 'Paid',
  '@t.dashboard.occupancy.caption@@': 'Occupancy',
  '@t.dashboard.occupancy.occupied_caption@@': 'Occupied',
  '@t.dashboard.occupancy.vacant_caption@@': 'Vacant',
  '@t.dashboard.occupancy.total_caption@@': 'Total',
  '@t.dashboard.payments.caption@@': 'Payments',
  '@t.dashboard.chart.legend@@': `{key, select,
    due {Due}
    paid {Paid}
  }`,

  '@t.units.title@@': 'Units',
  '@t.units.add_button@@': 'Add new unit',
  '@t.units.blank_slate@@': 'No units created.',
  '@t.units.status@@': `{status, select, 
    occupied {Occupied}
    vacant {Vacant}
  }`,

  '@t.unit.title@@': 'Unit',
  '@t.unit.back_to_units@@': 'Back to units',
  '@t.unit.tenant_caption@@': 'Tenant',
  '@t.unit.vacant_caption@@': 'Unit is currently vacant',
  '@t.unit.vacant_info@@': 'Add a tenant to this unit to start a new tenancy.',
  '@t.unit.add_tenant@@': 'Add tenant',
  '@t.unit.lease_caption@@': 'Lease',
  '@t.unit.unit_caption@@': 'Unit',
  '@t.unit.delete_unit@@': 'Delete unit',
  '@t.unit.end_tenancy@@': 'End tenancy',
  '@t.unit.lease_title@@': 'Lease',
  '@t.unit.back_to_unit@@': 'Back to unit',
  '@t.unit.lease_label@@': 'Yearly lease',
  '@t.unit.tenant_title@@': 'Tenant',
  '@t.unit.tenant.name_label@@': 'Name',
  '@t.unit.tenant.email_label@@': 'Email',
  '@t.unit.tenant.phone_label@@': 'Phone',
  '@t.unit.tenant.validation_message@@': 'Please fill out name, email and phone.',
  '@t.unit.unit_title@@': 'Unit',
  '@t.unit.unit.size_label@@': 'Size',
  '@t.unit.unit.rooms_label@@': 'Rooms',
  '@t.unit.unit.utilities_label@@': 'Utilities',

  '@t.new_unit.title@@': 'New unit',
  '@t.new_unit.address_label@@': 'Address',
  '@t.new_unit.search_address@@': 'Search address',
  '@t.new_unit.validation_message@@': 'Please select an address from the dropdown.',

  '@t.user.caption@@': 'Select your prefered language',
}
