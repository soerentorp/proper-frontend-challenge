import {IntlProvider} from 'react-intl'
import messages from 'locales'
import {ApolloProvider} from '@apollo/client'
import {useApollo} from 'lib/apolloClient'
import 'stylesheets/reset.css'
import 'stylesheets/global.css'
import {useRouter} from 'next/router'

const MyApp = ({Component, pageProps}) => {
  const apolloClient = useApollo()
  const router = useRouter()

  return (
    <div>
      <link rel="stylesheet" href="https://use.typekit.net/peo5nex.css" />

      <ApolloProvider client={apolloClient}>
        <IntlProvider messages={messages[router.locale]} locale={router.locale} defaultLocale={router.defaultLocale}>
          <Component {...pageProps} />
        </IntlProvider>
      </ApolloProvider>
    </div>
  )
}

export default MyApp
