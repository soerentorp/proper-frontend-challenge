import {ApolloServer, gql} from 'apollo-server-micro'
import knex from 'knex'
import knexStringcase from 'knex-stringcase'
import paymentHistory from 'data/paymentHistory'

const options = knexStringcase({
  client: 'pg',
  connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    ssl: {rejectUnauthorized: false},
  },
})

const db = knex(options)

const typeDefs = gql`
  type Query {
    units: [Unit!]!
    unit(id: ID!): Unit
    dashboard: Dashboard!
  }

  type Unit {
    id: ID!
    addressName: String!
    tenantName: String
    tenantEmail: String
    tenantPhone: String
    outstanding: Int
    leaseYearly: Int
    size: Int
    rooms: Int
    utilities: String
    status: String
  }

  type PaymentHistoryItem {
    month: String!
    due: Int!
    paid: Int!
  }

  type Dashboard {
    id: ID!
    paymentHistory: [PaymentHistoryItem!]!
    dueAmount: Float!
    billedAmount: Float!
    paidAmount: Float!
    occupancyRate: Float!
    occupiedCount: Float!
    vacantCount: Int!
    totalCount: Int!
    currency: String!
  }

  type Mutation {
    createUnit(addressName: String!, rooms: Int): Unit
    deleteUnit(id: ID!): Unit
    updateTenant(unitId: ID!, input: TenantInput!): Unit
    endTenancy(unitId: ID!): Unit
    updateLease(unitId: ID!, input: LeaseInput!): Unit
    updateUnit(unitId: ID!, input: UnitInput!): Unit
  }

  input TenantInput {
    name: String!
    email: String!
    phone: String!
  }

  input LeaseInput {
    leaseYearly: Float!
  }

  input UnitInput {
    size: Int
    rooms: Int
    utilities: String
  }
`

const resolvers = {
  Query: {
    units(parent, args, context) {
      return context.db.select('*').from('units')
    },
    unit(parent, args, context) {
      return findUnit(context.db, args.id)
    },
    async dashboard(parent, args, context) {
      const billedAmount = 42819
      const paidAmount = 34641
      const dueAmount = billedAmount - paidAmount
      const units = await context.db.select('*').from('units')
      const vacantCount = units.filter((u) => u.status === 'vacant').length
      const occupiedCount = units.filter((u) => u.status === 'occupied').length
      const totalCount = units.length
      const occupancyRate = occupiedCount / totalCount
      return {
        id: 'dashboard',
        paymentHistory,
        billedAmount,
        paidAmount,
        dueAmount,
        occupancyRate,
        occupiedCount,
        vacantCount,
        totalCount,
        currency: 'DKK',
      }
    },
  },
  Mutation: {
    async createUnit(parent, args, context) {
      const {addressName} = args
      const response = await context.db('units').insert({addressName}, 'id')
      const id = response[0]
      const units = await context.db
        .select('*')
        .from('units')
        .where({id})
      return units[0]
    },
    async deleteUnit(parent, args, context) {
      const {id} = args
      await context
        .db('units')
        .where({id})
        .del()
      return {id}
    },
    async updateTenant(parent, args, context) {
      const {unitId} = args
      await context
        .db('units')
        .where({id: unitId})
        .update({
          tenantName: args.input.name,
          tenantEmail: args.input.email,
          tenantPhone: args.input.phone,
          status: 'occupied',
        })
      return findUnit(context.db, unitId)
    },
    async endTenancy(parent, args, context) {
      const {unitId} = args
      await context
        .db('units')
        .where({id: unitId})
        .update({
          tenantName: null,
          tenantEmail: null,
          tenantPhone: null,
          status: 'vacant',
        })
      return findUnit(context.db, unitId)
    },
    async updateLease(parent, args, context) {
      const {unitId} = args
      await context
        .db('units')
        .where({id: unitId})
        .update({
          leaseYearly: args.input.leaseYearly,
        })
      return findUnit(context.db, unitId)
    },
    async updateUnit(parent, args, context) {
      const {unitId} = args
      await context
        .db('units')
        .where({id: unitId})
        .update({
          size: args.input.size,
          rooms: args.input.rooms,
          utilities: args.input.utilities,
        })
      return findUnit(context.db, unitId)
    },
  },
}

const findUnit = async (db, id) => {
  const units = await db
    .select('*')
    .from('units')
    .where({id})
  return units.length > 0 ? units[0] : null
}

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: () => ({
    db,
  }),
})

export const config = {
  api: {
    bodyParser: false,
  },
}

export default apolloServer.createHandler({path: '/api/graphql'})
