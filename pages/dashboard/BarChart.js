import {ResponsiveBar} from '@nivo/bar'
import {useIntl} from 'react-intl'
import {useText} from 'hooks'
import THEME from 'theme'

const MyResponsiveBar = ({data /* see data tab */}) => {
  const intl = useIntl()
  return (
    <ResponsiveBar
      data={data}
      keys={['paid', 'due']}
      indexBy="month"
      margin={{top: 40, right: 0, bottom: 40, left: 50}}
      padding={0.4}
      valueScale={{type: 'linear'}}
      colors={({id, data}) => ({due: '#ddd', paid: THEME.green}[id])}
      axisBottom={{
        format: (d) => `${intl.formatDate(new Date(d), {month: 'short'})}`,
      }}
      axisLeft={{
        format: (d) => `${d / 1000} tkr.`,
      }}
      labelTextColor={{from: 'color', modifiers: [['darker', 1.6]]}}
      label={(d) => {
        return ''
      }}
      animate={true}
      motionStiffness={90}
      motionDamping={15}
      tooltip={(item) => {
        const value = `${intl.formatNumber(item.value)} kr`
        const legend = useText({id: '@t.dashboard.chart.legend@@', values: {key: item.id}})
        return (
          <div>
            {legend}: {value}
          </div>
        )
      }}
      theme={{
        fontSize: 14,
        fontFamily: "'proxima-nova', 'Proxima Nova', sans-serif",
        textColor: '#666',
      }}
    />
  )
}

export default MyResponsiveBar
