import Link from 'next/link'
import {FormattedNumber} from 'react-intl'
import BarChart from './dashboard/BarChart'
import {gql, useQuery} from '@apollo/client'
import {Head, Heading, Card, Page, Text, Loader} from 'components'
import THEME from 'theme'

const GET_DASHBOARD = gql`
  query {
    dashboard {
      id
      paymentHistory {
        month
        due
        paid
      }
      dueAmount
      billedAmount
      paidAmount
      occupancyRate
      occupiedCount
      vacantCount
      totalCount
      currency
    }
  }
`

const DashboardPage = () => {
  const query = useQuery(GET_DASHBOARD)

  return (
    <Page>
      <Head title={{id: '@t.dashboard.title@@'}} />
      {query.loading ? <Loader /> : query.data && <Dashboard dashboard={query.data.dashboard} />}
    </Page>
  )
}

const Dashboard = (props) => {
  const {dashboard} = props
  return (
    <div>
      <div className="stats">
        <Card padding={48}>
          <Text bold text={{id: '@t.dashboard.rent_due.caption@@'}} size="large" weight="bold" />
          <Text size="huger" weight="fat" marginTop={12}>
            <FormattedNumber value={dashboard.dueAmount} style="currency" currency={dashboard.currency} />
          </Text>
          <div className="stat-footer">
            <div>
              <Text text={{id: '@t.dashboard.rent_due.billed_caption@@'}} weight="bold" />
              <Text marginTop={4}>
                <FormattedNumber value={dashboard.billedAmount} style="currency" currency={dashboard.currency} />
              </Text>
            </div>
            <div>
              <Text text={{id: '@t.dashboard.rent_due.paid_caption@@'}} weight="bold" />
              <Text marginTop={4}>
                <FormattedNumber value={dashboard.paidAmount} style="currency" currency={dashboard.currency} />
              </Text>
            </div>
          </div>
        </Card>
        <Card padding={48}>
          <Text bold text={{id: '@t.dashboard.occupancy.caption@@'}} size="large" weight="bold" />
          <Text size="huger" weight="fat" marginTop={12}>
            <FormattedNumber
              value={dashboard.occupancyRate}
              style="percent"
              minimumFractionDigits={0}
              maximumFractionDigits={0}
            />
          </Text>
          <div className="stat-footer">
            <div>
              <Text text={{id: '@t.dashboard.occupancy.occupied_caption@@'}} weight="bold" />
              <Text marginTop={4}>
                <FormattedNumber value={dashboard.occupiedCount} />
              </Text>
            </div>
            <div>
              <Text text={{id: '@t.dashboard.occupancy.vacant_caption@@'}} weight="bold" />
              <Text marginTop={4}>
                <FormattedNumber value={dashboard.vacantCount} />
              </Text>
            </div>
            <div>
              <Text text={{id: '@t.dashboard.occupancy.total_caption@@'}} weight="bold" />
              <Text marginTop={4}>
                <FormattedNumber value={dashboard.totalCount} />
              </Text>
            </div>
          </div>
        </Card>
        <div className="payments">
          <Card padding={48}>
            <Text bold text={{id: '@t.dashboard.payments.caption@@'}} size="large" weight="bold" />
            <div style={{height: 400}}>
              <BarChart data={dashboard.paymentHistory} />
            </div>
          </Card>
        </div>
      </div>
      <style jsx>{`
        .stats {
          display: grid;
          grid-gap: 24px;
          grid-template-columns: 1fr 1fr;
        }
        .stat-footer {
          margin-top: 24px;
          display: grid;
          grid-gap: 24px;
          grid-template-columns: 1fr 1fr 1fr;
        }
        .payments {
          grid-column: 1/3;
        }
      `}</style>
    </div>
  )
}
export default DashboardPage
