import {gql, useQuery} from '@apollo/client'

const PING = gql`
  query {
    ping
  }
`

const Ping = (props) => {
  const query = useQuery(PING)
  if (query.loading) {
    return <div>Pinging ... </div>
  } else {
    return <div>Response: {query.data.ping}</div>
  }
}

export default Ping
