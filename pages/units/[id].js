import {useRouter} from 'next/router'
import {gql, useQuery, useApolloClient} from '@apollo/client'
import {Head, Heading, Page, Text, Button, TextField, LinkButton, Loader, Label, Card, NotFound} from 'components'
import clearApolloCache from 'lib/clearApolloCache'
import {FormattedNumber} from 'react-intl'
import THEME from 'theme'

const GET_UNIT = gql`
  query Unit($id: ID!) {
    unit(id: $id) {
      id
      addressName
      tenantName
      tenantEmail
      tenantPhone
      outstanding
      leaseYearly
      size
      rooms
      utilities
      status
    }
  }
`

const DELETE_UNIT = gql`
  mutation DeleteUnit($id: ID!) {
    deleteUnit(id: $id) {
      id
    }
  }
`

const END_TENANCY = gql`
  mutation EndTenancy($unitId: ID!) {
    endTenancy(unitId: $unitId) {
      id
      tenantName
      tenantEmail
      tenantPhone
      status
    }
  }
`

const UnitPage = (props) => {
  const router = useRouter()
  const {id} = router.query
  const query = useQuery(GET_UNIT, {variables: {id}})

  return (
    <Page>
      <Head title={{id: '@t.unit.title@@'}} />
      <LinkButton text={{id: '@t.unit.back_to_units@@'}} href="/units" />
      {query.loading ? <Loader /> : query.data && <Unit unit={query.data.unit} />}
    </Page>
  )
}

const Unit = (props) => {
  const router = useRouter()
  const client = useApolloClient()
  const {unit} = props

  if (!unit) {
    return <NotFound marginTop={24} />
  }

  const handleDelete = () => {
    client
      .mutate({
        mutation: DELETE_UNIT,
        variables: {
          id: unit.id,
        },
      })
      .then(({data}) => {
        clearApolloCache(client.cache)
        router.push('/units')
      })
  }

  const handleEndTenancy = () => {
    client
      .mutate({
        mutation: END_TENANCY,
        variables: {
          unitId: unit.id,
        },
      })
      .then(({data}) => {
        clearApolloCache(client.cache)
      })
  }

  return (
    <div>
      <Heading text={unit.addressName} marginTop={32} marginBottom={48} />
      {unit.status === 'occupied' ? (
        <Card>
          <div className="box">
            <div>
              <Text text={{id: '@t.unit.tenant_caption@@'}} size="large" weight="bold" />
              <Text marginTop={12} text={unit.tenantName} />
              <Text marginTop={4} text={unit.tenantEmail} />
              <Text marginTop={6} text={unit.tenantPhone} />
            </div>
            <LinkButton text="Edit" href={`/units/${unit.id}/tenant/edit`} />
          </div>
        </Card>
      ) : (
        <Card>
          <div className="box">
            <div>
              <Text text={{id: '@t.unit.vacant_caption@@'}} size="large" weight="bold" />
              <Text text={{id: '@t.unit.vacant_info@@'}} marginTop={12} />
            </div>
            <LinkButton text={{id: '@t.unit.add_tenant@@'}} href={`/units/${unit.id}/tenant/edit`} type="primary" />
          </div>
        </Card>
      )}
      <Card marginTop={24}>
        <div className="box">
          <div>
            <Text text={{id: '@t.unit.lease_caption@@'}} size="large" weight="bold" />
            <Text marginTop={12}>
              <FormattedNumber value={unit.leaseYearly} style="currency" currency={'DKK'} />
            </Text>
          </div>
          <LinkButton text={{id: '@t.shared.edit@@'}} href={`/units/${unit.id}/lease/edit`} />
        </div>
      </Card>
      <Card marginTop={24}>
        <div className="box">
          <div>
            <Text text={{id: '@t.unit.unit_caption@@'}} size="large" weight="bold" />
            <Label text={{id: '@t.unit.unit.size_label@@'}} marginTop={12} />
            <Text text={unit.size || '-'} marginTop={4} />
            <Label text={{id: '@t.unit.unit.rooms_label@@'}} marginTop={12} />
            <Text text={unit.rooms || '-'} marginTop={4} />
            <Label text={{id: '@t.unit.unit.utilities_label@@'}} marginTop={12} />
            <Text text={unit.utilities || '-'} marginTop={4} />
          </div>
          <LinkButton text={{id: '@t.shared.edit@@'}} href={`/units/${unit.id}/unit/edit`} />
        </div>
      </Card>
      <div className="footer-actions">
        <Button text={{id: '@t.unit.delete_unit@@'}} onClick={handleDelete} marginRight={16} />
        {unit.status === 'occupied' && <Button text={{id: '@t.unit.end_tenancy@@'}} onClick={handleEndTenancy} />}
      </div>
      <style jsx>{`
        .caption {
          font-weight: bold;
          font-size: 24px;
        }
        .box {
          padding: 24px;
          display: flex;
          justify-content: space-between;
        }
        .footer-actions {
          margin-top: 24px;
          display: flex;
        }
      `}</style>
    </div>
  )
}

export default UnitPage
