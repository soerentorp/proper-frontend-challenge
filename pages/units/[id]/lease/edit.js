import React from 'react'
import {useRouter} from 'next/router'
import {gql, useQuery, useApolloClient} from '@apollo/client'
import {Head, Heading, Page, Text, Button, TextField, LinkButton, Loader, Label, FormCard} from 'components'

const GET_UNIT = gql`
  query Unit($id: ID!) {
    unit(id: $id) {
      id
      leaseYearly
    }
  }
`

const UPDATE_LEASE = gql`
  mutation UpdateLease($unitId: ID!, $input: LeaseInput!) {
    updateLease(unitId: $unitId, input: $input) {
      id
      leaseYearly
    }
  }
`

const EditLeasePage = (props) => {
  const router = useRouter()
  const {id} = router.query
  const query = useQuery(GET_UNIT, {variables: {id}})

  return (
    <Page>
      <Head title={{id: '@t.unit.lease_title@@'}} />
      <LinkButton text={{id: '@t.unit.back_to_unit@@'}} href={`/units/${id}`} />
      {query.loading ? <Loader /> : query.data && <Form unit={query.data.unit} />}
    </Page>
  )
}

const Form = (props) => {
  const client = useApolloClient()
  const router = useRouter()
  const {unit} = props
  const [leaseYearly, setLeaseYearly] = React.useState(unit.leaseYearly || '')

  const handleSubmit = () => {
    client
      .mutate({
        mutation: UPDATE_LEASE,
        variables: {
          unitId: unit.id,
          input: {
            leaseYearly: Number(leaseYearly),
          },
        },
      })
      .then(({data}) => {
        router.push(`/units/${unit.id}`)
      })
  }

  return (
    <FormCard>
      <Label text={{id: '@t.unit.lease_label@@'}} />
      <TextField value={leaseYearly} onChange={(e) => setLeaseYearly(e.target.value)} marginTop={8} />
      <Button text={{id: '@t.shared.save@@'}} onClick={handleSubmit} marginTop={32} type="primary" />
    </FormCard>
  )
}

export default EditLeasePage
