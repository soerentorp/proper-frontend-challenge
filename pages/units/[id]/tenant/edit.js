import React from 'react'
import {useRouter} from 'next/router'
import {gql, useQuery, useApolloClient} from '@apollo/client'
import {Head, Heading, Page, Text, Button, TextField, LinkButton, Loader, Label, FormCard} from 'components'
import clearApolloCache from 'lib/clearApolloCache'
import {useIntl} from 'react-intl'

const GET_UNIT = gql`
  query Unit($id: ID!) {
    unit(id: $id) {
      id
      tenantName
      tenantEmail
      tenantPhone
      status
    }
  }
`

const UPDATE_TENANT = gql`
  mutation UpdateTenant($unitId: ID!, $input: TenantInput!) {
    updateTenant(unitId: $unitId, input: $input) {
      id
      tenantName
      tenantEmail
      tenantPhone
      status
    }
  }
`

const EditTenantPage = (props) => {
  const router = useRouter()
  const {id} = router.query
  const query = useQuery(GET_UNIT, {variables: {id}})

  return (
    <Page>
      <Head title={{id: '@t.unit.tenant_title@@'}} />
      <LinkButton text={{id: '@t.unit.back_to_unit@@'}} href={`/units/${id}`} />
      {query.loading ? <Loader /> : query.data && <Form unit={query.data.unit} />}
    </Page>
  )
}

const Form = (props) => {
  const client = useApolloClient()
  const router = useRouter()
  const intl = useIntl()
  const {unit} = props
  const [name, setName] = React.useState(unit.tenantName || '')
  const [email, setEmail] = React.useState(unit.tenantEmail || '')
  const [phone, setPhone] = React.useState(unit.tenantPhone || '')

  const handleSubmit = () => {
    if (!(name && email && phone)) {
      const validationMessage = intl.formatMessage({id: '@t.unit.tenant.validation_message@@'})
      alert(validationMessage)
      return
    }
    client
      .mutate({
        mutation: UPDATE_TENANT,
        variables: {
          unitId: unit.id,
          input: {
            name,
            email,
            phone,
          },
        },
      })
      .then(({data}) => {
        clearApolloCache(client.cache)
        router.push(`/units/${unit.id}`)
      })
  }

  return (
    <FormCard>
      <Label text={{id: '@t.unit.tenant.name_label@@'}} />
      <TextField value={name} onChange={(e) => setName(e.target.value)} marginTop={8} />
      <Label text={{id: '@t.unit.tenant.email_label@@'}} marginTop={20} />
      <TextField value={email} onChange={(e) => setEmail(e.target.value)} marginTop={8} />
      <Label text={{id: '@t.unit.tenant.phone_label@@'}} marginTop={20} />
      <TextField value={phone} onChange={(e) => setPhone(e.target.value)} marginTop={8} />
      <Button text={{id: '@t.shared.save@@'}} onClick={handleSubmit} marginTop={32} type="primary" />
    </FormCard>
  )
}

export default EditTenantPage
