import React from 'react'
import {useRouter} from 'next/router'
import {gql, useQuery, useApolloClient} from '@apollo/client'
import {Head, Heading, Page, Text, Button, TextField, LinkButton, Loader, Label, FormCard} from 'components'

const GET_UNIT = gql`
  query Unit($id: ID!) {
    unit(id: $id) {
      id
      size
      rooms
      utilities
    }
  }
`

const UPDATE_UNIT = gql`
  mutation UpdateUnit($unitId: ID!, $input: UnitInput!) {
    updateUnit(unitId: $unitId, input: $input) {
      id
      size
      rooms
      utilities
    }
  }
`

const EditUnitPage = (props) => {
  const router = useRouter()
  const {id} = router.query
  const query = useQuery(GET_UNIT, {variables: {id}})

  return (
    <Page>
      <Head title={{id: '@t.unit.unit_title@@'}} />
      <LinkButton text={{id: '@t.unit.back_to_unit@@'}} href={`/units/${id}`} />
      {query.loading ? <Loader /> : query.data && <Form unit={query.data.unit} />}
    </Page>
  )
}

const Form = (props) => {
  const client = useApolloClient()
  const router = useRouter()
  const {unit} = props
  const [size, setSize] = React.useState(unit.size || '')
  const [rooms, setRooms] = React.useState(unit.rooms || '')
  const [utilities, setUtilities] = React.useState(unit.utilities || '')

  const handleSubmit = () => {
    client
      .mutate({
        mutation: UPDATE_UNIT,
        variables: {
          unitId: unit.id,
          input: {
            size: Number(size),
            rooms: Number(rooms),
            utilities,
          },
        },
      })
      .then(({data}) => {
        router.push(`/units/${unit.id}`)
      })
  }

  return (
    <FormCard>
      <Label text={{id: '@t.unit.unit.size_label@@'}} />
      <TextField value={size} onChange={(e) => setSize(e.target.value)} marginTop={8} />
      <Label text={{id: '@t.unit.unit.rooms_label@@'}} marginTop={20} />
      <TextField value={rooms} onChange={(e) => setRooms(e.target.value)} marginTop={8} />
      <Label text={{id: '@t.unit.unit.utilities_label@@'}} marginTop={20} />
      <TextField value={utilities} onChange={(e) => setUtilities(e.target.value)} marginTop={8} />
      <Button text={{id: '@t.shared.save@@'}} onClick={handleSubmit} marginTop={32} type="primary" />
    </FormCard>
  )
}

export default EditUnitPage
