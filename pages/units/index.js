import Link from 'next/link'
import {Head, Heading, Page, Text, LinkButton, Loader, Card} from 'components'
import {gql, useQuery} from '@apollo/client'
import _ from 'underscore'
import THEME from 'theme'

const GET_UNITS = gql`
  query {
    units {
      id
      addressName
      tenantName
      tenantEmail
      tenantPhone
      outstanding
      leaseYearly
      size
      rooms
      utilities
      status
    }
  }
`

const UnitsPage = (props) => {
  const query = useQuery(GET_UNITS)

  return (
    <Page>
      <Head title={{id: '@t.units.title@@'}} />
      <div className="main-header">
        <Heading text={{id: '@t.units.title@@'}} />
        <LinkButton text={{id: '@t.units.add_button@@'}} href="/units/new" />
      </div>
      {query.loading ? <Loader /> : query.data && <Units units={query.data.units} />}
      <style jsx>{`
        .main-header {
          display: flex;
          justify-content: space-between;
        }
      `}</style>
    </Page>
  )
}

const Units = (props) => {
  const units = _.sortBy(props.units, 'addressName')

  if (units.length === 0) {
    return (
      <Card padding={40} marginTop={40}>
        <Text text={{id: '@t.units.blank_slate@@'}} size="larger" />
      </Card>
    )
  }

  return (
    <div className="units">
      {units.map((unit) => {
        return (
          <Link key={unit.id} href={`/units/${unit.id}`}>
            <div className="unit">
              <div>
                <Text text={unit.addressName} size="large" weight="bold" />
                <Text text={unit.tenantName} marginTop={8} />
              </div>
              <div className={`status ${unit.status}`}>
                <Text text={{id: '@t.units.status@@', values: {status: unit.status}}} weight="bold" />
              </div>
            </div>
          </Link>
        )
      })}
      <style jsx>{`
        .units {
          margin-top: 48px;
        }
        .unit {
          display: flex;
          justify-content: space-between;
          align-items: center;
          margin-top: 12px;
          padding: 24px;
          border: solid 1px ${THEME.borderColor};
          border-radius: 8px;
          transition: all 0.25s ease-in-out;
          cursor: pointer;
        }
        .unit:hover {
          border-color: ${THEME.black};
        }
        .unit:active {
          background-color: ${THEME.black};
          color: white;
        }
        .unit-name {
          font-weight: bold;
          font-size: 18px;
        }
        .status {
          display: flex;
          align-items: center;
          padding: 8px 12px;
          color: white;
          border-radius: 8px;
        }
        .status.occupied {
          background-color: ${THEME.green};
        }
        .status.vacant {
          background-color: ${THEME.red};
        }
      `}</style>
    </div>
  )
}

export default UnitsPage
