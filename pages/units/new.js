import React from 'react'
import Link from 'next/link'
import Autosuggest from 'react-autosuggest'
import {Head, Heading, Page, Text, Button, TextField, LinkButton, Label, FormCard} from 'components'
import {gql, useApolloClient} from '@apollo/client'
import {useRouter} from 'next/router'
import clearApolloCache from 'lib/clearApolloCache'
import {useText} from 'hooks'
import THEME from 'theme'
import {useIntl} from 'react-intl'

const CREATE_UNIT = gql`
  mutation CreateUnit($addressName: String!) {
    createUnit(addressName: $addressName) {
      id
      addressName
    }
  }
`

const searchAddressesAsync = (value) => {
  return fetch(`https://dawa.aws.dk/adresser/autocomplete?q=${value}`, {
    method: 'GET',
  }).then((request) => {
    return request.json()
  })
}

const NewUnit = (props) => {
  const client = useApolloClient()
  const router = useRouter()
  const intl = useIntl()
  const [value, setValue] = React.useState('')
  const [addressName, setAddressName] = React.useState('')
  const [suggestions, setSuggestions] = React.useState([])

  const onChange = (event, {newValue}) => {
    setValue(newValue)
  }

  const onSuggestionsFetchRequested = ({value}) => {
    searchAddressesAsync(value).then((json) => {
      setSuggestions(json)
    })
  }

  const onSuggestionsClearRequested = () => {
    setSuggestions([])
  }

  const handleSubmit = () => {
    if (!addressName) {
      const validationMessage = intl.formatMessage({id: '@t.new_unit.validation_message@@'})
      alert(validationMessage)
      return
    }
    client
      .mutate({
        mutation: CREATE_UNIT,
        variables: {
          addressName,
        },
      })
      .then(({data}) => {
        clearApolloCache(client.cache)
        router.push(`/units/${data.createUnit.id}`)
      })
  }

  const handleSuggestionSelected = (event, {suggestion}) => {
    setAddressName(suggestion.tekst)
  }

  return (
    <Page>
      <Head title={{id: '@t.new_unit.title@@'}} />
      <LinkButton text={{id: '@t.unit.back_to_units@@'}} href="/units" />
      <Heading text={{id: '@t.new_unit.title@@'}} marginTop={32} />
      <FormCard>
        <Label text={{id: '@t.new_unit.address_label@@'}} marginBottom={12} />
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={onSuggestionsFetchRequested}
          onSuggestionsClearRequested={onSuggestionsClearRequested}
          onSuggestionSelected={handleSuggestionSelected}
          getSuggestionValue={(suggestion) => suggestion.tekst}
          renderSuggestion={(suggestion) => {
            return <div className="suggestion">{suggestion.tekst}</div>
          }}
          inputProps={{
            placeholder: useText({id: '@t.new_unit.search_address@@'}),
            value,
            onChange,
          }}
          theme={autosuggestTheme}
        />
        <Button type="primary" text={{id: '@t.shared.create@@'}} onClick={handleSubmit} marginTop={24} />
      </FormCard>
      <style jsx>{`
        .suggestion {
          padding: 12px;
          cursor: pointer;
        }
      `}</style>
    </Page>
  )
}

const autosuggestTheme = {
  container: {
    position: 'relative',
  },
  suggestionsContainer: {
    position: 'absolute',
    top: 48,
    width: '100%',
  },

  suggestionsContainerOpen: {
    backgroundColor: 'white',
    border: `solid 1px ${THEME.borderColor}`,
  },
  input: {
    fontSize: 21,
    fontFamily: THEME.fontFamily,
    border: `solid 1px ${THEME.borderColor}`,
    borderRadius: 4,
    WebkitAppearance: 'none',
    height: 48,
    paddingRight: 12,
    paddingLeft: 12,
    minWidth: 400,
  },
  inputOpen: {},
  suggestionHighlighted: {
    backgroundColor: '#eee',
  },
}

export default NewUnit
