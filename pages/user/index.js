import {Page, Avatar, Text, LinkButton, Heading} from 'components'
import Link from 'next/link'
import {useRouter} from 'next/router'

const User = (props) => {
  const router = useRouter()
  return (
    <Page>
      <div className="heading">
        <Heading text={{id: '@t.user.caption@@'}} />
      </div>
      <div className="locales">
        <div className="option">
          <LinkButton type="secondary" href="/user" locale="da" selected={router.locale === 'da'} text="Dansk" />
        </div>
        <div className="option">
          <LinkButton type="secondary" href="/user" locale="en" selected={router.locale === 'en'} text="English" />
        </div>
      </div>
      <style jsx>
        {`
          .heading {
            display: flex;
            justify-content: center;
            text-align: center;
          }
          .locales {
            display: flex;
            justify-content: center;
            margin-top: 24px;
          }
          .option {
            margin: 24px 8px;
          }
        `}
      </style>
    </Page>
  )
}

export default User
