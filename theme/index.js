export default {
  borderColor: '#dadada',
  red: '#ff003c',
  green: '#00c176',
  black: '#000',
  fontFamily: "'proxima-nova', 'Proxima Nova', sans-serif",
  fontSize: {
    smaller: 11,
    small: 14,
    regular: 16,
    large: 21,
    larger: 24,
    largest: 32,
    huge: 36,
    huger: 48,
    hugest: 72,
  },
  fontWeight: {
    regular: 500,
    bold: 700,
    fat: 800,
  },
}
